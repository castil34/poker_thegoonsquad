from django.shortcuts import render, redirect
from django.views import View
from app.goon_poker import *
from app.scoreboard import HighScore
import app.accounts as accounts

messages_to_display = 10


class CommandView(View):
    def __init__(self):
        super().__init__()
        self.messages = []

    def get(self, request):
        self.messages = request.session.get('messages', [])
        messages = reversed(self.get_messages(messages_to_display))
        login_name = request.session.get("name", "")
        return render(request, "command.html", {"message_list": messages, "login_name": login_name})

    def post(self, request):
        username = request.POST["user"]
        command_input = request.POST["command"]
        self.messages = request.session.get('messages', [])

        assert isinstance(command_input, str)
        if command_input:
            self.run_command(username, command_input, request.session)

        request.session['messages'] = self.messages
        return redirect('command')

    def message(self):
        return self.messages[-1]

    def get_messages(self, n):
        return self.messages[-n:]

    def run_command(self, name, command, session=None):
        """Parse and run a command"""

        command = command.strip()
        value = ""

        if not command:
            return

        if not name:
            if session and "name" in session and session["name"]:
                name = session["name"]
            else:
                self.messages.append("Error: no username provided")
                return

        if not accounts.has_account(name):
            response = "Error: user " + name + " not found. You may request a username from the administrator."
            self.messages.append(response)
            return

        def commandIs(text, takesParameter=False):
            if takesParameter:
                if command.startswith(text):
                    nonlocal value
                    value = command[len(text):].lstrip()
                    return True

            elif text == command:
                return True

            return False

        # Admin commands -----------------------------------------------------------------------------------------------
        if name == "admin":
            if commandIs("add user", True):
                values = value.split()
                if not values:
                    response = "Error: no username provided"
                elif len(values) == 1:
                    response = "Error: no password provided"
                else:
                    (username, password) = values

                    if username == "admin":
                        response = "Error: username admin is not allowed"
                    elif accounts.add_user(username, password):
                        response = "admin: added user " + username
                    else:
                        response = "Error: user " + username + " already exists"

                self.messages.append(response)
                return

            elif commandIs("remove user", True):
                if value == "":
                    response = "Error: no username provided"
                elif value == "admin":
                    response = "Error: admin cannot be removed"
                elif accounts.remove_user(value):
                    response = "admin: removed user " + value
                else:
                    response = "Error: user " + value + " already exists"

                self.messages.append(response)
                return

            elif commandIs("create table", True):
                if value == "":
                    response = "Error: no table name provided"
                elif goon_poker.create_table(value):
                    response = "admin: table " + value + " created"
                else:
                    response = "Error: table " + value + " already exists"

                self.messages.append(response)
                return

            elif commandIs("remove table", True):
                if value == "":
                    response = "Error: no table name provided"
                elif goon_poker.remove_table(value):
                    response = "admin: removed table " + value
                else:
                    response = "Error: table " + value + " doesn't exist"

                self.messages.append(response)
                return

            elif commandIs("reset scoreboard"):
                HighScore().reset()
                self.messages.append("admin: reset scoreboard")
                return

        # User commands ------------------------------------------------------------------------------------------------
        if commandIs("login", True):
            if accounts.login(name, value, session):
                response = name + ": logged in"
            else:
                response = "Error: invalid password"

        elif commandIs("logout"):
            if accounts.logout(name, session):
                response = name + ": logged out"
            else:
                response = "Error: " + name + " is not logged in"

        elif commandIs("say", True):
            response = name + ": says " + value

        elif commandIs("join table", True):
            if value == "":
                response = "Error: no table name provided"
            else:
                table = goon_poker.get_table(value)
                msg = ''
                if table:
                    old_table = goon_poker.table_of(name)
                    if old_table:
                        msg = old_table.leave_table(name)
                    response = table.add_player(name)
                    if msg != '':
                        response = response + '\n' + msg
                else:
                    response = "Error: table " + value + " doesn't exist"

        elif commandIs("open seats table", True):
            if value == "":
                response = "Error: no table name provided"
            else:
                seats = goon_poker.open_seats_at(value)

                if seats == -1:
                    response = "Error: table " + value + " doesn't exist"
                else:
                    response = "Table " + value + ": " + str(seats) + " open seats"

        elif commandIs("open seats"):
            all_seats = goon_poker.open_seats()

            if not all_seats:
                response = "No tables exist"
            else:
                lines = ["Table " + s[0] + ": " + str(s[1]) + " open seats" for s in all_seats]
                response = '\n'.join(lines)

        elif commandIs("players table", True):
            if value == "":
                response = "Error: no table name provided"
            else:
                players = goon_poker.players_at(value)
                if players:
                    response = ", ".join(players)
                else:
                    response = "None"

        elif commandIs("players"):
            lines = ["Table " + table + ": " + ", ".join(players) for (table, players) in goon_poker.players()]
            response = "\n".join(lines)

        elif commandIs("high scores"):
            response = str(HighScore())

        # Game-related commands ----------------------------------------------------------------------------------------
        else:
            try:
                table = goon_poker.table_of(name)

                if commandIs("raise", True):
                    if value == "":
                        response = "Error: no raise provided"
                    else:
                        response = table.raises(name, int(value))

                elif commandIs("call"):
                    response = table.call(name)

                elif commandIs("check"):
                    response = table.check(name)

                elif commandIs("fold"):
                    response = table.fold(name)

                elif commandIs("hand"):
                    response = table.see_hand(name)

                elif commandIs("chips"):
                    chips = table.get_player(name).chips
                    response = name + " has " + str(chips) + " chips"

                elif commandIs("total pot"):
                    response = "total pot is " + str(table.pot) + " chips"

                elif commandIs("leave table"):
                    response = table.leave_table(name)

                else:
                    response = "Error: unrecognized command"

            except AttributeError:
                response = "Error: " + name + " is not seated at a table"

        self.messages.append(response)
