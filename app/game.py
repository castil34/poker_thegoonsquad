from app.table import Table


class Game:
    def __init__(self):
        self.tables = []

    def create_table(self, name):
        if self.get_table(name):
            return False

        self.tables.append(Table(name))
        return True

    def remove_table(self, name):
        for i, table in enumerate(self.tables):
            if table.name == name:
                del self.tables[i]
                return True

        return False

    def join_table(self, user, name):
        try:
            table = self.get_table(name)
            old_table = self.table_of(user)
            if old_table:
                old_table.leave_table(user)
            return self.get_table(name).add_player(user)
        except AttributeError:
            return ""

    def get_table(self, name):
        for table in self.tables:
            if table.name == name:
                return table

    def table_names(self):
        return [table.name for table in self.tables]

    def open_seats_at(self, name):
        try:
            return self.get_table(name).get_open_seats()
        except AttributeError:
            return -1

    def open_seats(self):
        return [(table.name, table.get_open_seats()) for table in self.tables]

    def players_at(self, name):
        try:
            return self.get_table(name).get_player_names()
        except AttributeError:
            return None

    def players(self):
        return [(table.name, table.get_player_names()) for table in self.tables]

    def table_of(self, username):
        for table in self.tables:
            if table.get_player(username):
                return table
        return None

    # Parsing of table functions. In the future, table should be reformatted to return values, and the string messages
    # should be moved to CommandView.
    # Functions return an exception if username is not at a table
    def valid_moves(self, username):
        # preconditions: none
        # returns [check: Bool, raise: Bool, call: Bool, fold: Bool]
        table = self.table_of(username)
        return table.get_valid_moves(username)
        pass

    def raises(self, username, amount):
        # preconditions: the round has started, it is the player's turn, the player has enough chips, the amount is
        #     limited to the table max, the amount is greater than 0
        # returns [chips: Int, status: String]
        table = self.table_of(username)
        return table.raises(username, amount)
        pass

    def call(self, username):
        # preconditions: the round has started, it is the player's turn, the player has enough chips, there is a current
        #     raise in play
        # returns [chips: Int, status: String]
        table = self.table_of(username)
        return table.call(username)
        pass

    def check(self, username):
        # preconditions: the round has started, it is the player's turn, there is not a current raise in play
        # returns status: String
        table = self.table_of(username)
        return table.check(username)
        pass

    def fold(self, username):
        # preconditions: the round has started, the player has not yet folded
        # returns status: String
        table = self.table_of(username)
        return table.fold(username)
        pass

    def hand(self, username):
        # preconditions: the round has started, the player has not folded
        # returns [Card]
        pass

    def chips(self, username):
        # preconditions: none
        # returns chips: Int
        pass

    def pot(self, table_name):
        # preconditions: none
        # returns chips: Int
        pass

    def leave(self, username):
        # preconditions: none
        # no return value
        table = self.table_of(username)
        return table.leave_table(username)
        pass
