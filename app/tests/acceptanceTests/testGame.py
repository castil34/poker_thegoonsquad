from django.test import TestCase
from app.models import User
import app.pages.commandView as commandView
from app.game import Game
from app.scoreboard import HighScore
from django.utils import timezone

# Test commands related to overall game play, but not a specific poker game


# Requirement on every function:
# PBI: Player: I want to provide my username so that people know who I am.
# Criteria: When I as a player join a game, win a hand, or make a move, my name will be announced.

class TestGame(TestCase):
    def setUp(self):
        User.objects.create(name="mike")
        self.a = commandView.CommandView()
        commandView.goon_poker = Game()
        commandView.goon_poker.create_table("1")

    # PBI: Player: I want to be able to join an existing game. I want this so I can play with other players.
    # Criteria: When I type the command to join a game, I see a "game joined" when successful or a "no existing game"
    # when there is not
    def testJoinTableNoName(self):
        self.a.run_command("mike", "join table")
        self.assertEqual("Error: no table name provided", self.a.message())

    def testJoinTableExists(self):
        self.a.run_command("mike", "join table 1")
        self.assertEqual("mike has been added to the table. Waiting for one more player", self.a.message())

    def testJoinTableDoesntExist(self):
        self.a.run_command("mike", "join table 2")
        self.assertEqual("Error: table 2 doesn't exist", self.a.message())

    # PBI: Player: I want to be able to join an existing game. I want this so I can play with other players.
    def testOpenSeatsAtTableNoName(self):
        self.a.run_command("mike", "open seats table")
        self.assertEqual("Error: no table name provided", self.a.message())

    def testOpenSeatsAtTableExists(self):
        self.a.run_command("mike", "open seats table 1")
        self.assertEqual("Table 1: 4 open seats", self.a.message())

    def testOpenSeatsAtTableDoesntExist(self):
        self.a.run_command("mike", "open seats table 2")
        self.assertEqual("Error: table 2 doesn't exist", self.a.message())

    def testOpenSeatsNoTables(self):
        commandView.goon_poker.tables = []
        self.a.run_command("mike", "open seats")
        self.assertEqual("No tables exist", self.a.message())

    def testOpenSeatsOneTable(self):
        self.a.run_command("mike", "open seats")
        self.assertEqual("Table 1: 4 open seats", self.a.message())

    def testOpenSeatsMultipleTables(self):
        commandView.goon_poker.create_table("2")
        self.a.run_command("mike", "open seats")
        self.assertEqual("Table 1: 4 open seats\nTable 2: 4 open seats", self.a.message())

    def testViewScoreboard(self):
        today = str(timezone.now().date())
        scoreboard_str = ("100: AAA on " + today + "\n") * 5
        HighScore().reset()

        self.a.run_command("mike", "high scores")
        self.assertEqual(scoreboard_str, self.a.message())
