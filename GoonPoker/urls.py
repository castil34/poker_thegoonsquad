"""GoonPoker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from app.pages.commandView import CommandView
from app.views import *
from app.goon_poker import *    # loads the global game instance
from app.pages.tableView import TableView
from app.pages.tablesview import Tables
from app.pages.userviews import Users

urlpatterns = [
  re_path(r'^admin/', admin.site.urls),
  path('command/', CommandView.as_view(), name='command'),
  path('', Home.as_view(), name='home'),
  path('table/', TableView.as_view(), name='table'),
  path('register', Register.as_view(), name='register'),
  path('tables/', Tables.as_view(), name='tables'),
  path('users/', Users.as_view(), name='users'),
]
