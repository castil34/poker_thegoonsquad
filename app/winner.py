def poker_winner(hands):
    scores = []
    for hand in hands:
        scores = scores+[get_score(hand)]
    best_hand = scores[0]
    best_hand_loc = [0]
    for x in range(1, len(scores)):
        if best_hand[0] < scores[x][0]:
            best_hand = scores[x]
            best_hand_loc.clear()
            best_hand_loc.append(x)
        elif best_hand == scores[x]:
            best_hand_loc = best_hand_loc + [x]
        elif best_hand[0] == scores[x][0]:
            i = 0
            while scores[x][i] == best_hand[i] and i < len(best_hand):
                i = i + 1
            if scores[x][i] > best_hand[i]:
                best_hand = scores[x]
                best_hand_loc.clear()
                best_hand_loc.append(x)
    return best_hand_loc


def get_score(hand):
    if royal_flush(hand):
        return [10]
    elif straight_flush(hand):
        hand.sort(key=take_rank)
        return [9, hand[4].rank]
    elif four_of_a_kind(hand):
        hand.sort(key=take_rank)
        if hand[0].rank == hand[1].rank:
            return [8, hand[0].rank, hand[4].rank]
        return [8, hand[4].rank, hand[0].rank]
    elif full_house(hand):
        hand.sort(key=take_rank)
        if hand[2].rank == hand[3].rank:
            return [7, hand[2].rank, hand[0].rank]
        return [7, hand[2].rank, hand[4].rank]
    elif flush(hand):
        hand.sort(key=take_rank, reverse=True)
        ret_val = [6, hand[0].rank, hand[1].rank, hand[2].rank, hand[3].rank, hand[4].rank]
        return ret_val
    elif straight(hand):
        hand.sort(key=take_rank)
        return [5, hand[4].rank]
    elif three_of_a_kind(hand):
        hand.sort(key=take_rank)
        if hand[0].rank == hand[2].rank:
            return [4, hand[0].rank, hand[3].rank, hand[4].rank]
        elif hand[1].rank == hand[3].rank:
            return [4, hand[2].rank, hand[4].rank, hand[1].rank]
        return [4, hand[3].rank, hand[1].rank, hand[0].rank]
    elif two_pair(hand):
        hand.sort(key=take_rank)
        if hand[0].rank == hand[1].rank and hand[2].rank == hand[3].rank:
            return [3, hand[3].rank, hand[0].rank, hand[4].rank]
        elif hand[1].rank == hand[2].rank and hand[3].rank == hand[4].rank:
            return [3, hand[4].rank, hand[2].rank, hand[0].rank]
        return [3, hand[4].rank, hand[0].rank, hand[2].rank]
    elif pair(hand):
        hand.sort(key=take_rank)
        if hand[0].rank == hand[1].rank:
            return [2, hand[0].rank, hand[4].rank, hand[3].rank, hand[2].rank]
        if hand[1].rank == hand[2].rank:
            return [2, hand[1].rank, hand[4].rank, hand[3].rank, hand[0].rank]
        if hand[2].rank == hand[3].rank:
            return [2, hand[2].rank, hand[4].rank, hand[1].rank, hand[0].rank]
        return [2, hand[3].rank, hand[2].rank, hand[1].rank, hand[0].rank]
    else:
        hand.sort(key=take_rank, reverse=True)
        return [1, hand[0].rank, hand[1].rank, hand[2].rank, hand[3].rank, hand[4].rank]


def royal_flush(hand):
    hand.sort(key=take_rank)
    if straight_flush(hand) and hand[4].rank == 13:
        return True
    return False


def straight_flush(hand):
    if flush(hand) and straight(hand):
        return True
    return False


def four_of_a_kind(hand):
    hand.sort(key=take_rank)
    if distribution(hand) == 2:
        if hand[4].rank == hand[1].rank or hand[3].rank == hand[0].rank:
            return True
    return False


def full_house(hand):
    hand.sort(key=take_rank)
    if distribution(hand) == 2:
        if hand[4].rank == hand[2].rank or hand[0].rank == hand[2].rank:
            return True
    return False


def flush(hand):
    hand.sort(key=take_suit)
    if hand[4].suit == hand[0].suit:
        return True
    return False


def straight(hand):
    hand.sort(key=take_rank)
    if abs(hand[4].rank - hand[0].rank) == 4 and distribution(hand) == 5:
        return True
    return False


def two_pair(hand):
    hand.sort(key=take_rank)
    if distribution(hand) == 3:
        if not three_of_a_kind(hand):
            return True
    return False


def three_of_a_kind(hand):
    hand.sort(key=take_rank)
    if distribution(hand) == 3:
        if hand[0].rank == hand[2].rank or hand[1].rank == hand[3].rank or hand[2].rank == hand[4].rank:
            return True
    return False


def pair(hand):
    if distribution(hand) != 4:
        return False
    return True


def take_suit(card):
    return card.suit


def take_rank(card):
    return card.rank


def distribution(hand):
    distribution = {}
    for c in hand:
        if c.rank in distribution:
            distribution[c.rank] += 1
        else:
            distribution[c.rank] = 1
    return len(distribution)

