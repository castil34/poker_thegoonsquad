from django.test import TestCase
from app.card import Card


class MyTestCase(TestCase) :
    def setUp (self) :
        self.card = Card(1 , 2)

    def test_getSuit (self) :
        for i in range(1 , 4) :
            for j in range(1 , 13) :
                card = Card(i , j)
                self.assertEqual(card.getSuit() , i)

