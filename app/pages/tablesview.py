from django.views import View
from django.shortcuts import render, redirect
from app.accounts import *
from app.goon_poker import *


class Tables(View):
    def get(self, request):
        login_name = request.session.get("login_name", "")
        table_names = goon_poker.table_names()
        return render(request, "tables.html", {"login_name": login_name, "table_names": table_names})

    def post(self, request):
        opt = request.POST.get("opt", "")
        login_name = request.session.get("login_name", "")
        table_name = request.POST.get("table_name", "")

        if opt == "Add Table":
            goon_poker.create_table(table_name)
        elif opt == "Delete Table":
            goon_poker.remove_table(table_name)
        elif opt == "Logout":
            logout(login_name, request.session)
            return redirect("/")
        elif opt == "Users":
            return redirect("/users", {"login_name": login_name})
        table_names = goon_poker.table_names()
        return render(request, "tables.html", {"login_name": login_name, "table_names": table_names})
