from django.test import TestCase
from unittest.mock import Mock, MagicMock
from app.deck import Deck
from app.card import Card


def fake_deck():
    deck = Deck()
    deck.cards = []
    for k in range(1, 5):  # produces [1, 2, 3, 4] because range(n, m) is inclusive to exclusive
        for l in range(1, 14):
            deck.cards.append([k, l])
    return deck


# Deck tests
class DeckInit(TestCase):
    def setUp(self):
        self.deck = Deck()

    def test_init(self):
        self.assertEqual(self.deck.count(), 52, "You're not playing with a full deck")

        i = 0
        for suit in range(1, 5):
            for rank in range(1, 14):
                self.assertEqual(self.deck.cards[i].getSuit(), suit, "Cards initialized in the wrong order")
                self.assertEqual(self.deck.cards[i].getRank(), rank, "cards initialized in the wrong order")
                i += 1


class DeckCount(TestCase):
    def setUp(self):
        self.deck = fake_deck()

    def test_count(self):
        remaining_cards = 52
        # assert that we start with 52 "cards"
        self.assertEqual(self.deck.count(), 52, "You're not playing with a full deck to start with")

        for i in range(0, 51):
            self.deck.draw()
            remaining_cards -= 1
            self.assertEqual(self.deck.count(), remaining_cards, "Error in the deck count")


class DeckDrawValue(TestCase):
    def setUp(self):
        self.deck = fake_deck()

    def test_values(self):
        for i in range(0, 51):
            self.assertEqual(self.deck.cards[0], self.deck.draw(), "Draw card did not equal the top card")


class DeckDrawException(TestCase):
    def setUp(self):
        self.deck = fake_deck()

    def test_empty(self):
        for i in range(0, 52):
            self.deck.draw()

        with self.assertRaises(ValueError):
            self.deck.draw()


class DeckShuffle(TestCase):
    def setUp(self):
        self.myDeck = fake_deck()
        self.myDeck2 = fake_deck()

    def test_Shuffle(self):
        rptloc = 0
        threshold = 50  # will change later
        self.myDeck2.shuffle()

        count = 0
        for i in self.myDeck.cards:
            if i == self.myDeck2.cards[count]:
                rptloc = rptloc + 1
            count = count + 1
        self.assertLessEqual(rptloc, threshold, 'too many similarities in shuffled deck')
