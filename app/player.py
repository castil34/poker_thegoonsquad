class Player:
    def __init__(self, username):
        # Create a new game player with the given username
        self.name = username
        self.chips = 100
        self.folded = True
        self.amountRaised = 0
        self.status = "waiting"
        self.hand = []

    def __str__(self):
        return self.name

