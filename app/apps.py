from django.apps import AppConfig


class GoonPokerConfig(AppConfig):
    name = 'app'
    verbose_name = "Goon Poker"
