from app.card import Card
from Lib import random


class Deck:
    def __init__(self):
        # create a standard 52 card deck of cards
        self.cards = []
        for i in range(1, 5):
            for j in range(1, 14):
                self.cards.append(Card(i, j))

    def count(self):
        # return the number of cards remaining in the deck
        return len(self.cards)

    def draw(self):
        # return and remove the top card in the deck
        # if the deck is empty, raise a ValueError
        if self.count() <= 0:
            raise ValueError
        else:
            return self.cards.pop(0)

    def shuffle(self):
        # shuffle the deck using a random number generator
        return random.shuffle(self.cards)
