from django.test import TestCase
import app.pages.commandView as commandView
from app.models import User
import app.accounts as accounts

# Test commands unrelated to game play

# Requirement on every function:
# PBI: Player: I want to provide my username so that people know who I am.
# Criteria: When I as a player join a game, win a hand, or make a move, my name will be announced.


class TestApp(TestCase):
    def setUp(self):
        User.objects.create(name="nathan", password="password")
        User.objects.create(name="mike", password="password")
        self.mike = User.objects.get(name="mike")
        self.session = self.client.session
        self.a = commandView.CommandView()

    def testInvalidCommand(self):
        self.a.run_command("mike", "not a command")
        self.assertEqual("Error: unrecognized command", self.a.message())

    def testMissingNameWhileNotLoggedIn(self):
        self.a.run_command("", "say test")
        self.assertEqual("Error: no username provided", self.a.message())

    def testMissingNameWhileLoggedIn(self):
        accounts.login("mike", "password", self.session)
        self.a.run_command("", "say test", self.session)
        self.assertEqual("mike: says test", self.a.message())

    def testUserNameOverridesLoggedInUser(self):
        accounts.login("nathan", "password", self.session)
        self.a.run_command("mike", "say test", self.session)
        self.assertEqual("mike: says test", self.a.message())

    def testUserNameInvalid(self):
        self.a.run_command("ben", "say test")
        self.assertEqual("Error: user ben not found. You may request a username from the administrator.",
                         self.a.message())

    # PBI: Player: I want to provide my username so that people know who I am.
    def testLoginWhileNotLoggedIn(self):
        self.a.run_command("mike", "login password", self.session)
        self.assertEqual("mike: logged in", self.a.message())
        self.assertEqual("mike", self.session["name"])
        self.mike.refresh_from_db()
        self.assertTrue(self.mike.logged_in)

    def testLoginWhileLoggedIn(self):
        accounts.login("mike", "password", self.session)
        self.a.run_command("mike", "login password", self.session)
        self.assertEqual("mike: logged in", self.a.message())
        self.assertEqual("mike", self.session["name"])
        self.mike.refresh_from_db()
        self.assertTrue(self.mike.logged_in)

    def testLoginWhileSomeoneElseLoggedIn(self):
        accounts.login("nathan", "password", self.session)
        self.a.run_command("mike", "login password", self.session)
        self.assertEqual("mike: logged in", self.a.message())
        self.assertEqual("mike", self.session["name"])
        self.mike.refresh_from_db()
        self.assertTrue(self.mike.logged_in)

    def testLogoutWhileLoggedIn(self):
        accounts.login("mike", "password", self.session)
        self.a.run_command("mike", "logout", self.session)
        self.assertEqual("mike: logged out", self.a.message())
        self.assertEqual("", self.session["name"])
        self.assertFalse(self.mike.logged_in)

    def testLogoutWhileSomeoneElseLoggedIn(self):
        accounts.login("nathan", "password", self.session)
        self.a.run_command("mike", "logout", self.session)
        self.assertEqual("Error: mike is not logged in", self.a.message())
        self.assertEqual("nathan", self.session["name"])
        self.assertTrue(User.objects.get(name="nathan").logged_in)

    def testLogoutWhileNotLoggedIn(self):
        self.a.run_command("mike", "logout", self.session)
        self.assertEqual("Error: mike is not logged in", self.a.message())

    def testSay(self):
        self.a.run_command("mike", "say Hello, World!")
        self.assertEqual("mike: says Hello, World!", self.a.message())
