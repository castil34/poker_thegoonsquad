import unittest
from app.table import Table
from app.player import Player
from unittest.mock import MagicMock
from django.test import TestCase


class Check(TestCase):
    def setUp(self):
        self.table = Table("table1")
        self.table.add_player("Player1")
        self.table.add_player("Player2")
        self.table.add_player("Player3")
        self.table.check("Player1")
        self.table.check("Player2")

    def test_check(self):
        self.table.check("Player1")
        self.table.check("Player2")
        self.assertEqual(self.table.players[0].chips, 100, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips, 100, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[2].chips, 100, msg="Player has wrong number of chips")
        self.assertEqual(self.table.pot, 0, msg="Pot contains wrong number of chips")

    def test_check_not_your_turn(self):
        response = self.table.check("Player2")
        self.assertEqual('it is not your turn yet', response, msg="player called when it wasn't their turn")
        self.assertEqual(self.table.players[0].chips, 100, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips, 100, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[2].chips, 100, msg="Player has wrong number of chips")
        self.assertEqual(self.table.pot, 0, msg="Pot contains wrong number of chips")

    def test_check_raised(self):
        self.table.raises("Player1", 5)
        response = self.table.check("Player2")
        self.assertEqual('You must call, raise, or fold', response, msg="player called when it wasn't their turn")
        self.assertEqual(self.table.players[0].chips, 95, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips, 100, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[2].chips, 100, msg="Player has wrong number of chips")
        self.assertEqual(self.table.pot, 5, msg="Pot contains wrong number of chips")


class Raise(TestCase):
    def setUp(self):
        self.table = Table("table1")
        self.table.add_player("Player1")
        self.table.add_player("Player2")
        self.table.add_player("Player3")
        self.table.check("Player1")
        self.table.check("Player2")

    def test_raise(self):
        self.table.raises("Player1", 5)
        self.assertEqual(self.table.players[0].chips, 95, msg="Player has wrong number of chips")
        self.assertEqual(self.table.pot, 5, msg="Pot contains wrong number of chips")
        self.assertEqual(self.table.players[1].chips, 100, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[2].chips, 100, msg="Player has wrong number of chips")

    def test_raise2(self):
        self.table.raises("Player1", 5)
        self.table.raises("Player2", 5)
        self.table.raises("Player3", 5)
        self.table.raises("Player1", 5)
        self.table.raises("Player2", 5)
        self.table.raises("Player3", 5)
        self.table.call("Player1")
        self.assertEqual(self.table.players[0].chips, 70, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips, 75, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[2].chips, 70, msg="Player has wrong number of chips")
        self.assertEqual(self.table.pot, 85, msg="Pot contains wrong number of chips")
        self.table.call("Player2")
        self.assertEqual(self.table.players[0].chips + self.table.players[1].chips + self.table.players[2].chips, 300, msg="Chips lost during betting")

    def test_raise_not_your_turn(self):
        response = self.table.raises("Player2", 5)
        self.assertEqual('it is not your turn yet', response, msg="player called when it wasn't their turn")
        self.assertEqual(100, self.table.players[0].chips, "current player chips changed with improper call")
        self.assertEqual(100, self.table.players[1].chips, "chips changed with improper call")
        self.assertEqual(100, self.table.players[2].chips, "chips changed with improper call")
        self.assertEqual(0, self.table.pot, "pot increased")

    def test_raise_large(self):
        response = self.table.raises("Player1", 6)  # raising when not your turn
        self.assertEqual('Bet is too large for this table', response)
        self.assertEqual(100, self.table.players[0].chips, "current player chips changed with improper call")
        self.assertEqual(100, self.table.players[1].chips, "chips changed with improper call")
        self.assertEqual(0, self.table.pot, "pot increased")

    def test_raise_negative(self):
        response = self.table.raises("Player1", -2)  # raising when not your turn
        self.assertEqual('Your raise must be between 1 and 5', response)
        self.assertEqual(100, self.table.players[0].chips, "current player chips changed with improper call")
        self.assertEqual(100, self.table.players[1].chips, "chips changed with improper call")
        self.assertEqual(0, self.table.pot, "pot increased")

    def test_raise_zero(self):
        response = self.table.raises("Player1", 0)  # raising when not your turn
        self.assertEqual('Cannot raise 0 chips, this would be a call', response)
        self.assertEqual(100, self.table.players[0].chips, "current player chips changed with improper call")
        self.assertEqual(100, self.table.players[1].chips, "chips changed with improper call")
        self.assertEqual(0, self.table.pot, "pot increased")

    def test_raise_over_bet(self):
        self.table.players[0].chips = 1
        self.table.players[1].chips = 100
        response = self.table.raises(self.table.players[0].name, 5)
        self.assertEqual(response, "You do not have enough chips for this raise", msg="player overbet")


class Call(TestCase):
    def setUp(self):
        self.table = Table("table1")
        self.table.add_player("Player1")
        self.table.add_player("Player2")
        self.table.add_player("Player3")
        self.table.check("Player1")
        self.table.check("Player2")

    def test_call_when_no_raise(self):
        response = self.table.call("Player1")
        self.assertEqual('You must raise, check, or fold', response, msg="player called when nobody raised")
        self.assertEqual(100, self.table.players[0].chips, "chips increased with improper call")
        self.assertEqual(0, self.table.pot, "pot increased")

    def test_call_not_your_turn(self):
        response = self.table.call("Player2")
        self.assertEqual('it is not your turn yet', response, msg="player called when it wasn't their turn")
        self.assertEqual(100, self.table.players[0].chips, "current player chips changed with improper call")
        self.assertEqual(100, self.table.players[1].chips, "chips changed with improper call")
        self.assertEqual(0, self.table.pot, "pot increased")

    def test_call_over_bet(self):
        self.table.raises("Player1", 5)
        self.table.players[1].chips = 4
        response = self.table.call("Player2")
        self.assertEqual('You do not have enough chips to call', response, msg="player has wrong number of chips")
        self.assertEqual(95, self.table.players[0].chips, "current player chips changed with improper call")
        self.assertEqual(4, self.table.players[1].chips, "chips changed with improper call")
        self.assertEqual(5, self.table.pot, "pot increased")

    def test_call(self):
        self.table.raises("Player1", 5)
        response = self.table.call("Player2")
        self.assertEqual(95, self.table.players[0].chips, "current player chips changed with improper call")
        self.assertEqual(95, self.table.players[1].chips, "chips changed with improper call")
        self.assertEqual(100, self.table.players[2].chips, "chips changed with improper call")
        self.assertEqual(10, self.table.pot, "pot increased")


class Fold(TestCase):
    def setUp(self):
        self.table = Table("table1")
        self.table.add_player("Player1")
        self.table.add_player("Player2")

    def test_fold(self):
        self.table.raises(self.table.players[0].name, 5)
        self.assertEqual(95, self.table.players[0].chips, "wrong name")
        self.assertEqual(100, self.table.players[1].chips, "wrong name")
        response = self.table.fold(self.table.players[1].name)
        self.assertEqual(100, self.table.players[1].chips, "wrong name")
        self.assertEqual(self.table.players[1].folded, False, msg="Player still folded on new round")
        response = self.table.players[0].chips
        self.assertEqual(response, 100, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips, 100, msg="Player has wrong number of chips")

