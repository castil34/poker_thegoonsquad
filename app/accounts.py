# Django queries: https://docs.djangoproject.com/en/3.0/topics/db/queries/
# Model management: https://docs.djangoproject.com/en/3.0/ref/models/instances/

from app.models import User


def has_account(name):
    return User.objects.filter(name=name).exists()


def get_user(name):
    try:
        return list(User.objects.filter(name=name))[0]
    except IndexError:
        return None

def get_users():
    users = list(User.objects.all().order_by('name'))
    return [(user.name, user.logged_in) for user in users]


# Raises AttributeError if user doesn't exist
def login(name, password, session):
    user = get_user(name)
    if user.password != password:
        return False

    user.logged_in = True
    user.save()
    session['name'] = name
    return True


# Raises AttributeError if user doesn't exist
def logout(name, session):
    user = get_user(name)
    if not user.logged_in:
        return False

    if "name" not in session or not session["name"] or name != session["name"]:
        return False

    user.logged_in = False
    user.save()
    session['name'] = ""
    return True


def add_user(name, password):
    if has_account(name):
        return False

    User.objects.create(name=name, password=password)
    return True


def remove_user(name):
    maybe_user = User.objects.filter(name=name)

    if not maybe_user.exists():
        return False

    maybe_user.delete()
    return True
