from django.db import models
from datetime import datetime

MAX_NAME_LENGTH = 10
DEFAULT_HIGH = 100


class User(models.Model):
    name = models.CharField('user name', max_length=MAX_NAME_LENGTH, primary_key=True)
    password = models.CharField('password', max_length=MAX_NAME_LENGTH, default="password")
    logged_in = models.BooleanField('logged in', default=False)

    def __str__(self):
        return "user " + self.name


class Scoreboard(models.Model):
    score = models.IntegerField(default=DEFAULT_HIGH)
    name = models.CharField('user name', max_length=MAX_NAME_LENGTH)
    datetime = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.score) + ": " + self.name + " on " + str(self.datetime.date())
