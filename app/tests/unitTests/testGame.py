from django.test import TestCase
from app.models import User
from app.game import Game
from app.table import Table


class TestGame(TestCase):
    def setUp(self):
        User.objects.create(name="mike")
        self.game = Game()
        self.table1 = Table("1")
        self.game.tables = []
        self.game.tables.append(self.table1)

    def testCreateTableDoesntExist(self):
        self.assertTrue(self.game.create_table("2"))
        self.assertEqual(2, len(self.game.tables))
        self.assertEqual("2", self.game.tables[1].name)

    def testCreateTableExists(self):
        self.assertFalse(self.game.create_table("1"))

    def testRemoveTableExists(self):
        self.game.remove_table("1")
        self.assertEqual(0, len(self.game.tables))

    def testRemoveTableDoesntExist(self):
        self.assertFalse(self.game.remove_table("2"))

    def testJoinTableExists(self):
        self.assertTrue(self.game.join_table("mike", "1"))
        self.assertIn("mike", self.table1.get_player_names())

    def testJoinTableDoesntExist(self):
        self.assertFalse(self.game.join_table("mike", "2"))
        self.assertNotIn("mike", self.table1.get_player_names())

    def testGetTable(self):
        self.assertEqual("1", self.game.get_table("1").name)

    def testTableNames(self):
        self.assertEqual(["1"], self.game.table_names())

    def testOpenSeatsAtTableExists(self):
        self.assertEqual(4, self.game.open_seats_at("1"))

    def testOpenSeatsAtTableDoesntExist(self):
        self.assertEqual(-1, self.game.open_seats_at("2"))

    def testOpenSeatsNoTables(self):
        self.game.tables = []
        self.assertEqual([], self.game.open_seats())

    def testOpenSeatsOneTable(self):
        self.assertEqual([("1", 4)], self.game.open_seats())

    def testOpenSeatsMultipleTables(self):
        self.game.tables.append(Table("2"))
        self.assertEqual([("1", 4), ("2", 4)], self.game.open_seats())
