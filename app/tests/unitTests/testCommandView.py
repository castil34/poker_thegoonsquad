from django.test import TestCase
from app.pages.commandView import CommandView

# Since GoonPoker manages the textual interface, most functionality is directly covered in acceptance tests


class TestGoonPoker(TestCase):
    def setUp(self):
        self.a = CommandView()
        self.a.messages.append("test message")
        self.session = self.client.session

    def testMessage(self):
        self.assertEqual("test message", self.a.message())

    def testCommandWithNoNameNoSession(self):
        self.a.run_command("", "say test")
        self.assertEqual("Error: no username provided", self.a.message())

    def testCommandWithNoNameNoSessionName(self):
        self.a.run_command("", "say test", self.session)
        self.assertEqual("Error: no username provided", self.a.message())

    def testCommandWithNoNameEmptySessionName(self):
        self.session["name"] = ""
        self.a.run_command("", "say test", self.session)
        self.assertEqual("Error: no username provided", self.a.message())
