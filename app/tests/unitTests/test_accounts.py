from django.test import TestCase
from app.accounts import *


class TestAccounts(TestCase):
    def setUp(self):
        User.objects.create(name="mike", password="password")
        self.mike = User.objects.get(name="mike")
        self.session = self.client.session
        self.session["name"] = ""

    def testHasAccount(self):
        self.assertTrue(has_account(name="mike"))

    def testGetUserExists(self):
        self.assertTrue("mike", get_user("mike").name)

    def testGetUserDoesntExist(self):
        self.assertIsNone(get_user("nathan"))

    def testGetUsers(self):
        self.assertEqual([("mike", False)], get_users())

    def testLoginWhileNotLoggedIn(self):
        self.assertTrue(login("mike", "password", self.session))
        self.assertEqual("mike", self.session["name"])
        self.mike.refresh_from_db()
        self.assertTrue(self.mike.logged_in)

    def testLoginWhileLoggedIn(self):
        self.session["name"] = "mike"
        self.mike.logged_in = True
        self.mike.save()
        self.assertTrue(login("mike", "password", self.session))
        self.assertEqual("mike", self.session["name"])
        self.mike.refresh_from_db()
        self.assertTrue(self.mike.logged_in)

    def testLoginWrongPassword(self):
        self.assertFalse(login("mike", "pasword", self.session))
        self.assertNotEqual("mike", self.session["name"])
        self.mike.refresh_from_db()
        self.assertFalse(self.mike.logged_in)

    def testLoginWhileSomeoneElseLoggedIn(self):
        self.session["name"] = "nathan"
        self.assertTrue(login("mike", "password", self.session))
        self.assertEqual("mike", self.session["name"])
        self.mike.refresh_from_db()
        self.assertTrue(self.mike.logged_in)

    def testLogoutWhileLoggedIn(self):
        self.session["name"] = "mike"
        self.mike.logged_in = True
        self.mike.save()
        self.assertTrue(logout("mike", self.session))
        self.assertEqual("", self.session["name"])
        self.mike.refresh_from_db()
        self.assertFalse(self.mike.logged_in)

    def testLogoutWhileSomeoneElseLoggedIn(self):
        self.session["name"] = "nathan"
        self.assertFalse(logout("mike", self.session))
        self.assertEqual("nathan", self.session["name"])

    def testLogoutWhileNotLoggedIn(self):
        self.assertFalse(logout("mike", self.session))

    def testAddUserDoesntExist(self):
        self.assertTrue(add_user("nathan", "password"))
        nathan = User.objects.get(name="nathan")
        self.assertEqual("nathan", nathan.name)
        self.assertEqual("password", nathan.password)

    def testAddUserAlreadyExists(self):
        self.assertFalse(add_user("mike", "password2"))
        names = User.objects.filter(name="mike")
        self.assertEqual(1, len(names))
        self.assertEqual("password", self.mike.password)

    def testRemoveUserExists(self):
        self.assertTrue(remove_user("mike"))
        self.assertFalse(User.objects.filter(name="mike").exists())

    def testRemoveUserDoesntExist(self):
        self.assertFalse(remove_user("nathan"))
