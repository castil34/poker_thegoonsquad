from app.player import Player
from app.deck import Deck
import app.winner
from math import floor
from app.scoreboard import HighScore


class Table:
    def __init__(self, table_name):
        # create a new table with the given attributes
        self.name = table_name
        self.pot = 0
        self.playing = False
        self.seats = 4
        self.players = []
        self.currentPlayer = -2
        self.amountToRaise = 0
        self.turnsWithOutRaise = 0
        self.activePlayers = 0
        self.deck = []
        self.result = ""

    # functions that respond to users commands
    def raises(self, username, amount):
        if self.currentPlayer < 0:
            return "The round has not started yet"
        player = self.players[self.currentPlayer]
        if player.name != username:
            return 'it is not your turn yet'
        elif player.chips < amount:
            return 'You do not have enough chips for this raise'
        elif amount > 5:
            return 'Bet is too large for this table'
        elif amount == 0:
            return 'Cannot raise 0 chips, this would be a call'
        elif amount < 0:
            return 'Your raise must be between 1 and 5'
        else:
            player.status = "raise"
            self.amountToRaise = self.amountToRaise + amount
            player.chips = player.chips - self.amountToRaise + player.amountRaised
            self.pot = self.pot + self.amountToRaise - player.amountRaised
            player.amountRaised = self.amountToRaise
            message = username + ' ;  total chips: ' + str(player.chips) + ' ; Current Bet:' + str(player.amountRaised) + ' Status: ' + str(player.status)
            self.turnsWithOutRaise = 1
            val = self.next_player()
            return val + '\n' + message

    def call(self, username):
        if self.currentPlayer < 0:
            return "The round has not started yet"
        player = self.players[self.currentPlayer]
        if player.name != username:
            return 'it is not your turn yet'
        elif self.amountToRaise == 0:
            return 'You must raise, check, or fold'
        elif player.chips < self.amountToRaise - player.amountRaised:
            return 'You do not have enough chips to call'
        else:
            player.status = "call"
            player.chips = player.chips - self.amountToRaise + player.amountRaised
            self.pot = self.pot + self.amountToRaise - player.amountRaised
            player.amountRaised = self.amountToRaise
            message = str(username) + ' ;  total chips: ' + str(player.chips) + ' ; Current Bet:' + str(player.amountRaised) + ' Status: ' + str(player.status)
            self.turnsWithOutRaise = self.turnsWithOutRaise + 1
            val = self.next_player()
            return val + '\n' + message

    def check(self, username):
        if self.currentPlayer < 0:
            return "The round has not started yet"
        player = self.players[self.currentPlayer]
        if player.name != username:
            return 'it is not your turn yet'
        elif self.amountToRaise == player.amountRaised:
            player.status = "Check"
            message = str(username) + ' ;  total chips: ' + str(player.chips) + ' ; Current Bet:' + str(player.amountRaised) + ' Status: ' + str(player.status)
            self.turnsWithOutRaise = self.turnsWithOutRaise + 1
            val = self.next_player()
            return val + '\n' + message
        else:
            return 'You must call, raise, or fold'

    def fold(self, username):
        if self.currentPlayer < 0:
            return "The round has not started yet"
        player = self.players[self.currentPlayer]
        if not username == player.name:
            return "wrong player"
        player.folded = True
        player.status = "Fold"
        message = str(username) + ' ;  total chips: ' + str(player.chips) + ' ; Current Bet:' + str(player.amountRaised) + ' Status: ' + str(player.status)
        self.activePlayers = self.activePlayers - 1
        val = self.next_player()
        return val + '\n' + message

    def see_hand(self, username):
        player = self.get_player(username)
        return " ".join(str(card) for card in player.hand)

    def leave_table(self, username):
        player = self.get_player(username)
        index = self.get_player_index(username)
        if self.currentPlayer < 0:
            self.players.remove(player)
            return username + " has left the table"
        elif player is not None:
            if self.currentPlayer == index and index == len(self.players) - 1:
                self.fold(username)
            elif self.currentPlayer == index:
                if not player.folded:
                    self.activePlayers = self.activePlayers - 1
            elif index == len(self.players) - 1:
                self.activePlayers = self.activePlayers - 1
            elif self.currentPlayer < index:
                self.activePlayers = self.activePlayers - 1
            elif self.currentPlayer > index:
                self.activePlayers = self.activePlayers - 1
                self.currentPlayer = self.currentPlayer - 1
            self.players.remove(player)
            if len(self.players) <= 1:
                self.currentPlayer = -2
                self.playing = False
                return self.decide_winner() + '\n' + username + ' has left the table'
            return username + " has left the table. It is " + self.players[self.currentPlayer].name + "'s turn"
        else:
            return username + " is not at the table"

    # getters
    def get_player(self, name):
        for player in self.players:
            if player.name == name:
                return player
        return None

    def get_player_index(self, name):
        for x in range(0, len(self.players)):
            if self.players[x].name == name:
                return x
        return None

    def get_player_names(self):
        return [player.name for player in self.players]

    def get_seats(self):
        return self.seats

    def get_table_name(self):
        return self.name

    def get_open_seats(self):
        seats_available = self.seats - len(self.players)
        return seats_available

    def get_current_player(self):
        if self.currentPlayer == -1:
            return 'round has not started yet'
        else:
            return self.players[self.currentPlayer]

    def get_active_players(self):   # get players that have not folded
        active_players = [player for player in self.players if not player.folded]
        return active_players

    def get_hand(self, name):
        player = self.get_player(name)
        if player:
            return player.hand
        return None

    # returns [check, raise, call, fold]
    def get_valid_moves(self, name):
        if self.currentPlayer >= 0:
            player = self.players[self.currentPlayer]
            if player.name == name:
                if self.amountToRaise == player.amountRaised:
                    return [True, True, False, True]
                if self.amountToRaise > player.amountRaised:
                    return [False, True, True, True]
        return [False, False, False, False];

    # only used by Table.py
    def add_player(self, username):
        if len(self.players) < self.seats:
            new_player = Player(username)
            self.players.append(new_player)
            if len(self.players) > 1 and not self.playing:
                self.start_round()
                return username + ' has been added to the table and a round has started'
            elif len(self.players) < 2:
                return username + ' has been added to the table. Waiting for one more player'
            else:
                return username + ' has been added to the table and can play next round'

        else:
            return 'could not add ' + username + '. Table is full'

    def start_round(self):
        for x in range(0, len(self.players)):
            self.players[x].folded = False
            self.players[x].hand = []
            self.players[x].amountRaised = 0
        self.currentPlayer = 0
        self.amountToRaise = 0
        self.turnsWithOutRaise = 0
        self.activePlayers = len(self.players)
        self.deal()
        self.pot = 0
        self.playing = True
        return

    def deal(self):
        self.deck = Deck()
        self.deck.shuffle()
        for i in range(0, len(self.players)):
            for x in range(5):
                card_draw = self.deck.draw()
                self.players[i].hand.append(card_draw)
        return "cards have been dealt"

    def next_player(self):
        previous_player = self.players[self.currentPlayer]
        if self.currentPlayer == -2:
            return 'Game has not yet started'
        elif self.activePlayers == self.turnsWithOutRaise or self.activePlayers == 1:
            self.currentPlayer = -1  # next round has not started yet
            winner = self.decide_winner()
            return winner
        else:
            if self.currentPlayer < len(self.players) - 1:
                self.currentPlayer = self.currentPlayer + 1
            else:
                self.currentPlayer = 0
            while previous_player != self.players[self.currentPlayer] and self.players[self.currentPlayer].folded:
                if self.currentPlayer < len(self.players) - 1:
                    self.currentPlayer = self.currentPlayer + 1
                else:
                    self.currentPlayer = 0
            return 'It is '+str(self.players[self.currentPlayer].name)+"'s turn"

    def decide_winner(self):
        active_players = self.get_active_players()
        active_hands = [player.hand for player in self.players if not player.folded]
        winner = app.winner.poker_winner(active_hands)
        winners = "Winner: " + str(active_players[winner[0]].name)
        if len(winner) > 1:
            for x in range(1, len(winner)-1):
                winners = winners + ", " + str(active_players[winner[x]])
            winners = winners + ", " + str(active_players[winner[len(winner)-1]]) + "\n"
        hands = "\n"
        if len(active_hands) == 1:
            hands = ""
        for x in range(0, len(active_hands)):
            hands = hands + str(active_players[x].name) + ":  " + str(self.see_hand(active_players[x].name))+"\n"
        winnings = floor(self.pot / len(winner))
        self.result = hands + winners + "\nwinnings: " + str(winnings)

        for x in range(0, len(winner)):
            active_players[winner[x]].chips = active_players[winner[x]].chips + winnings
            # TODO: fix adding to high score
            # HighScore().update_score(active_players[winner[x]].name, active_players[winner[x]].chips)
        if len(self.players) > 1:
            self.start_round()
        else:
            self.playing = False
            self.currentPlayer = -2
        return self.result
