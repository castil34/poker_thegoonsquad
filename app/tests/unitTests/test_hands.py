from app.table import Table
from app.deck import Card
from django.test import TestCase


class CompHands(TestCase):
    royal_flush = [Card(1, 13), Card(1, 12), Card(1, 11), Card(1, 10), Card(1, 9)]
    straight_flush = [Card(1, 8), Card(1, 12), Card(1, 11), Card(1, 10), Card(1, 9)]
    four_of_a_kind = [Card(4, 10), Card(1, 10), Card(3, 10), Card(1, 10), Card(2, 9)]
    four_of_a_kind1 = [Card(4, 1), Card(1, 10), Card(3, 10), Card(1, 10), Card(2, 10)]
    full_house = [Card(4, 6), Card(1, 6), Card(3, 6), Card(1, 11), Card(2, 11)]
    flush = [Card(1, 2), Card(1, 7), Card(1, 11), Card(1, 4), Card(1, 9)]
    straight = [Card(2, 13), Card(4, 12), Card(1, 11), Card(3, 10), Card(1, 9)]
    three_of_a_kind = [Card(1, 8), Card(2, 8), Card(1, 8), Card(4, 2), Card(3, 5)]
    two_pair = [Card(4, 3), Card(1, 10), Card(3, 10), Card(1, 9), Card(2, 9)]
    pair = [Card(4, 9), Card(1, 2), Card(3, 6), Card(1, 11), Card(2, 11)]
    high_card = [Card(1, 2), Card(3, 7), Card(4, 11), Card(1, 4), Card(2, 9)]
    lower_high_card = [Card(1, 1), Card(3, 6), Card(4, 11), Card(1, 4), Card(2, 9)]

    def setUp(self):
        self.table = Table("table1")
        self.table.add_player("Player1")
        self.table.add_player("Player2")
        self.Player1 = self.table.players[0]
        self.Player2 = self.table.players[1]

    def test_same_hand(self):
        self.Player2.hand = self.two_pair
        self.Player1.hand = self.two_pair
        self.table.raises("Player1", 5)
        self.table.call("Player2")
        self.assertEqual(self.Player2.chips, 100, msg="Player has wrong number of chips")
        self.assertEqual(self.Player1.chips, 100, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips+self.table.players[0].chips, 200, msg="Player has wrong number of chips")

    def test_same_hand_hc(self):
        self.Player2.hand = self.high_card
        self.Player1.hand = self.lower_high_card
        self.table.raises("Player1", 5)
        self.table.call("Player2")
        self.assertEqual(self.table.players[1].chips + self.table.players[0].chips, 200,msg="Player has wrong number of chips")
        self.assertEqual(self.Player2.chips, 105, msg="Player has wrong number of chips")
        self.assertEqual(self.Player1.chips, 95, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips + self.table.players[0].chips, 200,msg="Player has wrong number of chips")

    def test_rf_sf(self):
        self.Player1.hand = self.royal_flush
        self.Player2.hand = self.straight_flush
        self.table.raises("Player1", 5)
        self.table.call("Player2")
        self.assertEqual(self.Player2.chips, 95, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips+self.table.players[0].chips, 200, msg="Player has wrong number of chips")

    def test_sf_4ok(self):
        self.Player2.hand = self.straight_flush
        self.Player1.hand = self.four_of_a_kind
        self.table.raises("Player1", 5)
        self.table.call("Player2")
        self.assertEqual(self.Player2.chips, 105, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips+self.table.players[0].chips, 200, msg="Player has wrong number of chips")

    def test_4ok_fh(self):
        self.Player2.hand = self.four_of_a_kind
        self.Player1.hand = self.full_house
        self.table.raises("Player1", 5)
        self.table.call("Player2")
        self.assertEqual(self.Player2.chips, 105, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips+self.table.players[0].chips, 200, msg="Player has wrong number of chips")

    def test_4ok2_fh1(self):
        self.Player2.hand = self.four_of_a_kind1
        self.Player1.hand = self.full_house
        self.table.raises("Player1", 5)
        self.table.call("Player2")
        self.assertEqual(self.Player2.chips, 105, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips+self.table.players[0].chips, 200, msg="Player has wrong number of chips")

    def test_fh_fl(self):
        self.Player1.hand = self.full_house
        self.Player2.hand = self.flush
        self.table.raises("Player1", 5)
        self.table.call("Player2")
        self.assertEqual(self.Player1.chips, 105, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips+self.table.players[0].chips, 200, msg="Player has wrong number of chips")

    def test_fl_st(self):
        self.Player1.hand = self.flush
        self.Player2.hand = self.straight
        self.table.raises("Player1", 5)
        self.table.call("Player2")
        self.assertEqual(self.Player1.chips, 105, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips+self.table.players[0].chips, 200, msg="Player has wrong number of chips")

    def test_st_3ok(self):
        self.Player2.hand = self.straight
        self.Player1.hand = self.three_of_a_kind
        self.table.raises("Player1", 5)
        self.table.call("Player2")
        self.assertEqual(self.Player2.chips, 105, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips+self.table.players[0].chips, 200, msg="Player has wrong number of chips")

    def test_3ok_tp(self):
        self.Player2.hand = self.three_of_a_kind
        self.Player1.hand = self.two_pair
        self.table.raises("Player1", 5)
        self.table.call("Player2")
        self.assertEqual(self.Player2.chips, 105, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips+self.table.players[0].chips, 200, msg="Player has wrong number of chips")

    def test_tp_pa(self):
        self.Player2.hand = self.two_pair
        self.Player1.hand = self.pair
        self.table.raises("Player1", 5)
        self.table.call("Player2")
        self.assertEqual(self.Player2.chips, 105, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips+self.table.players[0].chips, 200, msg="Player has wrong number of chips")

    def test_pa_hc(self):
        self.Player1.hand = self.pair
        self.Player2.hand = self.high_card
        self.table.raises("Player1", 5)
        self.table.call("Player2")
        self.assertEqual(self.Player1.chips, 105, msg="Player has wrong number of chips")
        self.assertEqual(self.table.players[1].chips+self.table.players[0].chips, 200, msg="Player has wrong number of chips")













