from django.test import TestCase
from app.pages.commandView import CommandView
from app.models import User


class CheckingUserName(TestCase):
    # name: Raunak
    # backlog: Player: I want to provide my username so that people know who I am.
    # Acceptance criteria: Each username is unique and is an active account
    def setUp(self):
        User.objects.create(name="admin")
        self.name = "goon"
        self.a = CommandView()

    def test_duplicates(self):
        self.a.run_command("admin", "add user " + self.name + " password")
        self.assertEqual("admin: added user " + self.name, self.a.message())

        # criteria: checking username exists in the the database
        self.assertTrue(User.objects.filter(name=self.name).exists())

        self.a.run_command("admin", "add user " + self.name + " password")
        # criteria: checking username is similar to the ones created if its similar then its not unique
        # as its not unique that means username already exists, and if not then the username is unique
        self.assertEqual("Error: user " + self.name + " already exists", self.a.message())
