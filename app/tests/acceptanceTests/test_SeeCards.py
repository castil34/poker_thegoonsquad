from django.test import TestCase
import app.pages.commandView as commandView
from app.game import Game
from app.models import User


# Johnathan Wise
# PBI: Player: I want to see my cards so that I know whether to bet, check or fold.
class test_SeeCard(TestCase):
    def setUp(self):
        User.objects.create(name="Johnathan")
        User.objects.create(name="Dave")
        User.objects.create(name="mike")

        self.a = commandView.CommandView()
        commandView.goon_poker = Game()
        commandView.goon_poker.create_table("1")
        commandView.goon_poker.join_table("Johnathan", "1")
        commandView.goon_poker.join_table("Dave", "1")
        commandView.goon_poker.join_table("mike", "1")
        self.table1 = commandView.goon_poker.get_table("1")
        # *** It's a secret to everybody
        self.a.run_command("Johnathan", "check")
        self.a.run_command("Dave", "check")
        self.hand = self.table1.see_hand("Johnathan")

    # Criteria: If a player folds they can still see their cards
    def test_show_hand_unfolded(self):
        self.a.run_command("Johnathan", "hand")
        response = self.a.message()
        self.assertEqual(response, self.hand, "Error: Cannot show cards")

    # Criteria: If a player folds they can still see their cards
    def test_show_hand_folded(self):
        self.a.run_command("Johnathan", "fold")
        self.a.run_command("Johnathan", "hand")
        response = self.a.message()
        self.assertEqual(response, self.hand, "Error: Cannot show cards")

    # # Criteria: Player cards cannot be seen by other players
    # def test_show_hand_diffuser(self):
    #     self.a.run_command("Dave", "see cards")
    #     response = self.a.message()
    #     self.assertEqual(response, "You cannot do that.", "Error: Player is seeing someone else's cards.")

    # Criteria: When cards shown are my cards
    def test_does_have_same_hand(self):
        self.a.run_command("Johnathan", "hand")
        response = self.a.message()
        self.assertEqual(response, self.hand, "Error: Did not receive same hand")
