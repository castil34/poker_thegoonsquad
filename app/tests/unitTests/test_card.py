from django.test import TestCase
from app.card import Card


def fake_deck():
    cards = []
    for k in range(1, 5):
        for l in range(1, 14):
            cards.append(Card(k, l))
    return cards


# Card tests
class CardInitCorrectRankAndSuit(TestCase):
    def setUp(self):
        self.card = Card(1, 2)

    def test_suit_init(self):
        self.assertEqual(self.card.getSuit(), 1, "Suit initialized with wrong value")

    def test_rank_init(self):
        self.assertEquals(self.card.getRank(), 2, "Rank initialized with wrong value")


class CardInitRaiseValueError(TestCase):
    def test_suit_error(self):
        errmsg = "Bad suit in initialization did not raise an exception"

        with self.assertRaises(ValueError, msg=errmsg):
            Card(-1, 1)
        with self.assertRaises(ValueError, msg=errmsg):
            Card(0, 1)
        with self.assertRaises(ValueError, msg=errmsg):
            Card(5, 1)

    def test_rank_error(self):
        errmsg = "Bad rank in initialization did not raise an exception"

        with self.assertRaises(ValueError, msg=errmsg):
            Card(1, -1)
        with self.assertRaises(ValueError, msg=errmsg):
            Card(1, 0)
        with self.assertRaises(ValueError, msg=errmsg):
            Card(1, 14)


class CardInitRaiseTypeErrorBadTypes(TestCase):
    def test_init(self):
        with self.assertRaises(TypeError, msg="Bad suit type did not raise an exception"):
            Card("A", 1)
        with self.assertRaises(TypeError, msg="Bad rank type did not raise an exception"):
            Card(1, "1")


class CardInitRaiseTypeError(TestCase):
    def test_missing_suit(self):
        with self.assertRaises(TypeError, msg="Missing suit did not raise an exception"):
            Card(rank=1)

    def test_missing_rank(self):
        with self.assertRaises(TypeError, msg="Missing rank did not raise an exception"):
            Card(suit=1)


class CardGetSuit(TestCase):
    def test_all(self):
        for i in range(1, 4):
            for j in range(1, 13):
                card = Card(i, j)
                self.assertEqual(card.getSuit(), i)


class CardGetRank(TestCase):
    def test_rank(self):
        for i in range(1, 4):
            for j in range(1, 13):
                card = Card(i, j)
                self.assertEqual(card.getRank(), j)


class CardGetValue(TestCase):
    def test_Ace(self):
        card = Card(1, 13)
        self.assertEqual(0, card.getValue(), "value should always be zero")

    def test_all(self):
        for i in range(1, 4):
            for j in range(1, 13):
                card = Card(i, j)
                self.assertEqual(card.getValue(), 0)


class CardStr(TestCase):
    def setUp(self):
        self.deck = fake_deck()

    def test_all(self):
        suits = ['C', 'D', 'H', 'S']
        ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']

        i = 0
        for s in range(0, 4):
            for r in range(0, 13):
                self.assertEqual(ranks[r] + suits[s], self.deck[i].__str__(), "Card string is incorrect")
                i += 1

