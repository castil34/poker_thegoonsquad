from django.test import TestCase
import app.pages.commandView as commandView
from app.game import Game
from app.models import User


# User Story: I want to be able to join an existing game, or if none exists, to start a new one. I want this so I can
# play with other players.
# Acceptance Criteria: A game is joined.
# Ben Messerschmidt
class TestGameStart(TestCase):
    def setUp(self):
        User.objects.create(name="admin")
        self.g = commandView.CommandView()
        commandView.goon_poker = Game()
        self.g.run_command("admin", "add user newbie password")
        self.g.run_command("admin", "add user second password")
        self.g.run_command("admin", "add user third password")
        self.g.run_command("admin", "add user fourth password")
        self.g.run_command("admin", "add user fifth password")


    # Test that game can't be joined when no table exists
    def test_no_existing_table(self):
        self.g.run_command("newbie", "join table 1")
        self.assertEqual("Error: table 1 doesn't exist", self.g.message())

    # Test that a player can't join a table when it is full
    def test_table_is_full(self):
        self.g.run_command("admin", "create table 1")
        self.g.run_command("second", "join table 1")
        self.g.run_command("third", "join table 1")
        self.g.run_command("fourth", "join table 1")
        self.g.run_command("fifth", "join table 1")
        self.g.run_command("newbie", "open seats")
        self.assertEqual("Table 1: 0 open seats", self.g.message())
        self.g.run_command("newbie", "join table 1")
        self.assertEqual("could not add newbie. Table is full", self.g.message())

        # Test that a player can join a table when there are open seats
    def test_join_table(self):
        self.g.run_command("admin", "create table 1")
        self.g.run_command("newbie", "open seats table 1")
        self.assertEqual("Table 1: 4 open seats", self.g.message())
        self.g.run_command("newbie", "join table 1")
        self.assertEqual("newbie has been added to the table. Waiting for one more player", self.g.message())

        # Test that a player move to a different table with open seats
    def test_move_table(self):
        self.g.run_command("admin", "create table 1")
        self.g.run_command("admin", "create table 2")
        self.g.run_command("newbie", "open seats table 1")
        self.assertEqual("Table 1: 4 open seats", self.g.message())
        self.g.run_command("newbie", "join table 1")
        self.g.run_command("newbie", "open seats table 2")
        self.assertEqual("Table 2: 4 open seats", self.g.message())
        self.g.run_command("newbie", "join table 2")
        self.assertEqual("newbie has been added to the table. Waiting for one more player\nnewbie has left the table",
                         self.g.message())
