from django.test import TestCase
import app.pages.commandView as commandView
from app.game import Game
from app.models import User


class TableUnitTesting(TestCase):
    def setUp(self):
        User.objects.create(name="admin")
        self.g = commandView.CommandView()
        commandView.goon_poker = Game()
        self.g.run_command("admin", "create table 1")
        self.g.run_command("admin", "add user first password")
        self.g.run_command("admin", "add user second password")
        self.g.run_command("admin", "add user third password")
        self.g.run_command("admin", "add user fourth password")
        self.g.run_command("admin", "add user fifth password")
        self.g.run_command("admin", "add user sixth password")

    def test_create_table(self):
        self.g.run_command("admin", "create table 2")
        self.assertEqual("admin: table 2 created", self.g.message())

    def test_empty_table(self):
        self.g.run_command("admin", "open seats table 1")
        self.assertEqual("Table 1: 4 open seats", self.g.message())

    def test_filling_table(self):
        self.g.run_command("first", "open seats table 1")
        self.assertEqual("Table 1: 4 open seats", self.g.message())
        self.g.run_command("first", "players table 1")
        self.assertEqual("None", self.g.message())
        self.g.run_command("first", "join table 1")
        self.g.run_command("second", "open seats table 1")
        self.assertEqual("Table 1: 3 open seats", self.g.message())
        self.g.run_command("second", "players table 1")
        self.assertEqual("first", self.g.message())
        self.g.run_command("second", "join table 1")
        self.g.run_command("third", "open seats table 1")
        self.assertEqual("Table 1: 2 open seats", self.g.message())
        self.g.run_command("third", "players table 1")
        self.assertEqual("first, second", self.g.message())
        self.g.run_command("third", "join table 1")
        self.g.run_command("fourth", "open seats table 1")
        self.assertEqual("Table 1: 1 open seats", self.g.message())
        self.g.run_command("fourth", "players table 1")
        self.assertEqual("first, second, third", self.g.message())
        self.g.run_command("fourth", "join table 1")
        self.g.run_command("fifth", "open seats table 1")
        self.assertEqual("Table 1: 0 open seats", self.g.message())
        self.g.run_command("fifth", "players table 1")
        self.assertEqual("first, second, third, fourth", self.g.message())
        self.g.run_command("fifth", "join table 1")
        self.g.run_command("sixth", "open seats table 1")
        self.assertEqual("Table 1: 0 open seats", self.g.message())
        self.g.run_command("sixth", "players table 1")
        self.assertEqual("first, second, third, fourth", self.g.message())
        self.g.run_command("sixth", "join table 1")
        self.assertEqual("could not add sixth. Table is full", self.g.message())
