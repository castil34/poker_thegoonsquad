from django.shortcuts import render, redirect
from django.views import View
from app.goon_poker import *
from app.accounts import *
from app.models import User
from app.accounts import add_user, remove_user, get_users, get_user, has_account


class Users(View):
    def get(self, request):
        login_name = request.session.get("login_name", "")
        if login_name != 'admin':
            message = "Must be Admin to see users"
            return redirect("/", {"message": message})
        users = list(User.objects.filter(logged_in=True))
        all_users = get_users()
        all_users.sort(reverse=True, key=lambda u: u[1])
        return render(request, "users.html", {"login_name": login_name, "users": users, "all_users": all_users})

    def post(self, request):
        opt = request.POST.get("opt", "")
        login_name = request.session.get("login_name", "")
        username = request.POST.get("username", "")
        password = request.POST.get("password", "")
        if login_name != 'admin':
            message = "Must be Admin to see users"
            return redirect("/", {"message": message})
        message = ""
        if opt == "Add User":
            if add_user(username, password):
                message = "User "+str(username)+" has been created"
        elif opt == "Delete User":
            if remove_user(username):
                message = "User " + str(username) + " has been deleted"
        elif opt == "Tables":
            return redirect("/tables")
        elif opt == "Logout":
            logout(login_name, request.session)
            return redirect("/")
        all_users = get_users()
        all_users.sort(reverse=True, key=lambda u: u[1])
        return render(request, "users.html", {"login_name": login_name, "all_users": all_users, "message": message})
