class Card:
    def __init__(self, suit, rank):
        # initialize a card to the given suit (1-4) and rank (1-13)
        # 1:clubs 2:diamonds 3:hearts 4:spades
        # raise an exception for invalid argument, ValueError
        if (suit < 1 or suit > 4) or (rank < 1 or rank > 13):
            raise ValueError
        self.suit = suit
        self.rank = rank
        self.value = 0
        pass

    def getSuit(self):
        # return the suit of the card (1-4)
        return self.suit

    def getRank(self):
        # return the rank of the card (1-13)
        return self.rank

    def getValue(self):
        # get the game-specific value of a Card
        # if not used, or game is not defined, always returns 0.
        return self.value

    def __str__(self):
        # return rank and suite, as AS for ace of spades, or 3H for
        # three of hearts, JC for jack of clubs.
        if self.getRank() == 13:
            r = "A"
        elif self.getRank() == 10:
            r = "J"
        elif self.getRank() == 11:
            r = "Q"
        elif self.getRank() == 12:
            r = "K"
        else:
            r = self.getRank() + 1

        if self.getSuit() == 1:
            s = "C"
        elif self.getSuit() == 2:
            s = "D"
        elif self.getSuit() == 3:
            s = "H"
        else:  # self.getSuit() == 4
            s = "S"
        return str(r) + s

    def __eq__(self, other):
        # Return whether two cards are equal
        if isinstance(other, Card):
            if self.getRank() == other.getRank() and self.getSuit() == other.getSuit():
                return True
        else:
            return False
