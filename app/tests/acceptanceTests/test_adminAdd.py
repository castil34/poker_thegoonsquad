from django.test import TestCase
from app.pages.commandView import CommandView
from app.models import User


# Nathan Von Allmen
# User Story:  As an admin, I want to create player accounts, so that that people can play the game.
# Size:  Medium
# Acceptance Criteria:  New accounts can only be made by an admin. Player names must be unique.
class AdminAddUser(TestCase):
    def setUp(self):
        User.objects.create(name="admin")
        User.objects.create(name="mike")
        self.a = CommandView()

    def test_make_user_as_admin(self):
        self.a.run_command("admin", "add user dave password")
        response = self.a.message()
        self.assertEqual(response, "admin: added user dave", "Error: user creation failed")

    def test_make_user_as_user(self):
        self.a.run_command("mike", "create user steve password")
        response = self.a.message()
        self.assertEqual(response, "Error: unrecognized command", "Error: a user was allowed to create another user")