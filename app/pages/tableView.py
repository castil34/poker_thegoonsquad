from django.views import View
from django.shortcuts import render, redirect
from app.goon_poker import *
from app.table import *
from app.models import User
from app.card import Card


class TableView(View):
    def get(self, request):
        request.session["name"] = ""
        login_name = request.GET.get("user", "")
        table_name = request.GET.get("tablename", "")
        request.session["login_name"] = login_name
        request.session["table_name"] = table_name
        # test values for debug
        enable = [False, False, False, False]
        table = goon_poker.table_of(table_name)
        goon_poker.join_table(login_name, table_name)
        offset = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100]
        return render(request, 'table.html', {"enable": enable, "message": "Welcome to the game!", "chips": str(100), "bet": str(0), "offset": offset, "pot": str(0), "table_name": table_name, "login_name": login_name})

    def post(self, request):
        opt = request.POST.get("opt", "")
        username = request.session["login_name"]
        table_name = request.session["table_name"]
        message = ""
        table = goon_poker.get_table(table_name)
        player_options = goon_poker.valid_moves(username)    # [check, raise, call, fold], get from table
        player_list = table.get_player_names()
        players = table.players
        my_chips = table.get_player(username).chips
        bet = table.get_player(username).amountRaised
        my_pot = table.pot
        my_hand = table.get_player(username).hand  # Card instances
        # translate into offset numbers for formatting: [width, height, ...]
        rotate = table.get_player_index(username)
        name_ordered = players[rotate:] + players[:rotate]
        offset = []
        for i in my_hand:
            rank = i.getRank() + 1
            if rank == 14:
                rank = 1
            if rank == 13 or rank == 12:
                # make the queen and king cards look a bit better
                offset.append(-49 * (rank - 1) - 1)
            else:
                offset.append(-49 * (rank - 1))
            offset.append(-64 * (i.getSuit() - 1))

        if opt == "Quit":
            goon_poker.leave(username)
            return redirect("/tables", {"name": username})
        elif opt == "Submit":
            message = "raise"
            bet = int(request.POST.get("raise", 0))
            if 5 >= bet >= 0:
                goon_poker.raises(username, bet)
            else:
                message = "Error: Bet must be between 0 and 5"
        elif opt == "Check":
            message = "check"
            goon_poker.check(username)
        elif opt == "Call":
            if my_chips >= bet:
                message = "call"
                goon_poker.call(username)
            else:
                message = "You do not have enough chips for this"
        elif opt == "Fold":
            message = username + " has folded"
            goon_poker.call(username)
        elif opt == "Refresh":
            pass

        v = {"enable": player_options, "message": message, "chips": str(my_chips), "bet": str(bet), "name": name_ordered, "offset": offset, "pot": my_pot, "table_name": table_name, "login_name": username}
        return render(request, "table.html", v)
