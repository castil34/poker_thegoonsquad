from django.test import TestCase
import app.pages.commandView as commandView
from app.game import Game
from app.models import User

# Test commands that only an admin can do

# Requirement on every function:
# PBI: Admin: I want to have a special user name so that I can be distinct from players.
# Criteria: When I (an admin) am in a game all players will be able to tell I am an admin.


class TestAdmin(TestCase):
    def setUp(self):
        User.objects.create(name="admin")
        User.objects.create(name="mike")
        self.a = commandView.CommandView()
        commandView.goon_poker = Game()
        commandView.goon_poker.create_table("1")

    # # PBI: Admin: I want to have a special username so that I can be distinct from players.
    # # Criteria: I can log in using the name "admin"
    # def testLogin(self):
    #     self.a.run_command("admin", "login")
    #     self.assertEqual("admin: logged in", self.a.message())

    # PBI: Admin: I want to have a special username so that I can be distinct from players.
    # Criteria: No one else is allowed to "admin" as a username
    def testCannotCreateAnotherAdmin(self):
        self.a.run_command("admin", "add user admin password")
        self.assertEqual("Error: username admin is not allowed", self.a.message())

    def testCannotDeleteAdmin(self):
        self.a.run_command("admin", "remove user admin")
        self.assertEqual("Error: admin cannot be removed", self.a.message())

    # PBI: Admin: I want to create player accounts so that people can play the game.
    # Criteria: I as an admin will be able to make new player accounts.
    def testAddUserNoName(self):
        self.a.run_command("admin", "add user")
        self.assertEqual("Error: no username provided", self.a.message())

    def testAddUserDoesntExistAsAdmin(self):
        self.a.run_command("admin", "add user goon password")
        self.assertEqual("admin: added user goon", self.a.message())

    def testAddUserExistsAsAdmin(self):
        self.a.run_command("admin", "add user mike password")
        self.assertEqual("Error: user mike already exists", self.a.message())

    # PBI: Admin: I want to create player accounts so that people can play the game.
    # Criteria: I as a player will not be able to make new player accounts.
    def testAddUserAsUser(self):
        self.a.run_command("mike", "add user goon password")
        self.assertEqual("Error: unrecognized command", self.a.message())

    def testRemoveUserNoName(self):
        self.a.run_command("admin", "remove user")
        self.assertEqual("Error: no username provided", self.a.message())

    def testRemoveUserAsAdmin(self):
        self.a.run_command("admin", "remove user mike")
        self.assertEqual("admin: removed user mike", self.a.message())

    def testRemoveUserAsUser(self):
        self.a.run_command("mike", "remove user nathan")
        self.assertEqual("Error: unrecognized command", self.a.message())

    # PBI: Admin: I want to create and delete tables, so that players can join games together
    # Criteria: I can create and name tables
    def testCreateTableNoName(self):
        self.a.run_command("admin", "create table")
        self.assertEqual("Error: no table name provided", self.a.message())

    def testCreateTableDoesntExistAsAdmin(self):
        self.a.run_command("admin", "create table 2")
        self.assertEqual("admin: table 2 created", self.a.message())

    def testCreateTableExistsAsAdmin(self):
        self.a.run_command("admin", "create table 1")
        self.assertEqual("Error: table 1 already exists", self.a.message())

    # PBI: Admin: I want to create and delete tables, so that players can join games together
    # Criteria: Users cannot create or name tables
    def testCreateTableAsUser(self):
        self.a.run_command("mike", "create table 1")
        self.assertEqual("Error: unrecognized command", self.a.message())

    # PBI: Admin: I want to create and delete tables, so that players can join games together
    # Criteria: I can delete tables
    def testRemoveTableNoName(self):
        self.a.run_command("admin", "remove table")
        self.assertEqual("Error: no table name provided", self.a.message())

    def testRemoveTableExistsAsAdmin(self):
        self.a.run_command("admin", "remove table 1")
        self.assertEqual("admin: removed table 1", self.a.message())

    def testRemoveTableDoesntExistAsAdmin(self):
        self.a.run_command("admin", "remove table 2")
        self.assertEqual("Error: table 2 doesn't exist", self.a.message())

    # PBI: Admin: I want to create and delete tables, so that players can join games together
    # Criteria: Users cannot delete tables
    def testRemoveTableAsUser(self):
        self.a.run_command("mike", "remove table 1")
        self.assertEqual("Error: unrecognized command", self.a.message())

    # Requirement: The admin can reset the high score list.
    def testResetScoreboardAsAdmin(self):
        self.a.run_command("admin", "reset scoreboard")
        self.assertEqual("admin: reset scoreboard", self.a.message())

    def testResetScoreboardAsUser(self):
        self.a.run_command("mike", "reset scoreboard")
        self.assertEqual("Error: unrecognized command", self.a.message())