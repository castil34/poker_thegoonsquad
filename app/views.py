from django.views import View
from django.shortcuts import render, redirect
from app.accounts import *
from . import game
from .models import User
from app.goon_poker import *


class Home(View):
    def get(self, request):
        request.session["login_name"] = ""
        return render(request, 'index.html', {"message": "Please Register if you have not already!"})

    def post(self, request):
        opt = request.POST.get("opt", "")
        if opt == "Register":
            return redirect("/register")
        message = ""
        name = request.POST.get("name", "")
        password = request.POST.get("password", "")
        names = list(User.objects.filter(name=name))
        # if name == "admin":
        #     if password == "password":
        #         request.session["name"] = name
        #     else:
        #         message = "Wrong password"
        if len(names) == 0:
            message = "User does not exist"
        else:
            # existing user
            u = names[0]
            if u.password == password:
                login(name, password, request.session)
                pass
            else:
                message = "Wrong password"
        if message == "":
            request.session["login_name"] = name
            return redirect("tables/")
        else:
            return render(request, "index.html", {"message": message})


class Register(View):
    def get(self, request):
        request.session["name"] = ""
        return render(request, "register.html", {"message": "Please enter your desired username!"})

    def post(self, request):
        opt = request.POST.get("opt", "/")
        if opt == "Login":
            return render(request, "index.html", {"message": "Welcome Back!"})
        message = ""
        name = request.POST.get("name", "")
        password = request.POST.get("password", "")
        password2 = request.POST.get("password2", "")
        if password != password2:
            message = "Passwords do not match!"
        names = list(User.objects.filter(name=name))
        if len(names) == 0:
            # new user
            if len(name) > 0:
                # valid
                add_user(name, password)
            else:
                message = "Invalid name"
        else:
            message = "Username already exists!"
        if message == "":
            message = name + " created successfully"
            request.session["name"] = name
            return render(request, "index.html", {"message": message})
        else:
            return render(request, "Register.html", {"message": message})


