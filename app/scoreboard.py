from app.models import Scoreboard


class HighScore:
    # create a local list of the high scores to test the new score,
    # sorted in descending order by score, then datetime
    def __init__(self):
        self.values = list(Scoreboard.objects.order_by('-score', 'datetime'))
        self.scores = []
        for i in self.values:
            self.scores.append(i.score)

    def update_score(self, name, score):
        found = False
        # find if the score given belongs to the high score list
        for i in self.scores:
            if score >= i:
                found = True
                break
        if found:
            # remove the lowest score from the database and the local list
            # that score is at index 4
            Scoreboard.objects.filter(score=self.scores[4]).delete()
            self.scores.remove(self.scores[4])
            # add the new score
            Scoreboard.objects.create(score=score, name=name)
            self.scores.append(score)
            self.scores.sort(reverse=True)

    def reset(self):
        # delete all scores from the database, clear local lists
        Scoreboard.objects.all().delete()
        self.values.clear()
        self.scores.clear()
        for i in range(0, 5):
            Scoreboard.objects.create(score=100, name="AAA")
        # update the local lists
        self.values = list(Scoreboard.objects.order_by('-score', 'datetime'))
        self.scores = []
        for i in self.values:
            self.scores.append(i.score)

    def __str__(self):
        tmp = ""
        for i in self.values:
            tmp_score = Scoreboard.objects.filter(score=i.score, name=i.name, datetime=i.datetime)[0]
            tmp += format(tmp_score.__str__())
            tmp += format("\n")
        return tmp
