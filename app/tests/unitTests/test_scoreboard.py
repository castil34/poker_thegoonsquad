from django.test import TestCase
from app.scoreboard import HighScore
from app.models import Scoreboard
from datetime import datetime


def setUpTestData(cls):
    Scoreboard.objects.create(score=105, name="Nathan")
    Scoreboard.objects.create(score=130, name="Mike")
    Scoreboard.objects.create(score=115, name="Raunak")
    Scoreboard.objects.create(score=110, name="Jacob")
    Scoreboard.objects.create(score=255, name="Ben")


class HighScoreInit(TestCase):
    def test_init(self):
        setUpTestData(cls=Scoreboard)
        # test correct order
        self.highScores = HighScore()
        self.assertEqual(self.highScores.scores[0], 255)
        self.assertEqual(self.highScores.scores[1], 130)
        self.assertEqual(self.highScores.scores[2], 115)
        self.assertEqual(self.highScores.scores[3], 110)
        self.assertEqual(self.highScores.scores[4], 105)


class HighScoreUpdateScore(TestCase):
    def test_update_high(self):
        # test that adding a high score adds the new score
        # and removes the lowest high score
        setUpTestData(cls=Scoreboard)
        self.highScores = HighScore()
        HighScore.update_score(self.highScores, "Johnathan", 150)
        self.assertEqual(self.highScores.scores[0], 255)
        self.assertEqual(self.highScores.scores[1], 150)
        self.assertEqual(self.highScores.scores[2], 130)
        self.assertEqual(self.highScores.scores[3], 115)
        self.assertEqual(self.highScores.scores[4], 110)

    def test_update_duplicate(self):
        # test that adding an equivalent score updates the database with the more recent score
        # and removes the lowest score
        setUpTestData(cls=Scoreboard)
        self.highScores = HighScore()
        HighScore.update_score(self.highScores, "Johnathan", 130)
        new_score = list(Scoreboard.objects.filter(name="Johnathan"))
        self.assertEqual(new_score[0].score, 130)
        self.assertEqual(self.highScores.scores[0], 255)
        self.assertEqual(self.highScores.scores[1], 130)
        self.assertEqual(self.highScores.scores[2], 130)
        self.assertEqual(self.highScores.scores[3], 115)
        self.assertEqual(self.highScores.scores[4], 110)

    def test_update_low(self):
        # test that trying to add a low score does not change anything
        setUpTestData(cls=Scoreboard)
        self.highScores = HighScore()
        HighScore.update_score(self.highScores, "Johnathan", 95)
        # test correct order
        self.assertEqual(self.highScores.scores[0], 255)
        self.assertEqual(self.highScores.scores[1], 130)
        self.assertEqual(self.highScores.scores[2], 115)
        self.assertEqual(self.highScores.scores[3], 110)
        self.assertEqual(self.highScores.scores[4], 105)


class HighScoreReset(TestCase):
    def test_reset(self):
        setUpTestData(cls=Scoreboard)
        self.highScores = HighScore()
        self.highScores.reset()
        my_values = list(Scoreboard.objects.order_by('-score', 'datetime'))
        for i in range(0, 5):
            self.assertEqual(self.highScores.scores[i], 100)
            self.assertEqual(my_values[i].name, "AAA")
            

class HighScoreStr(TestCase):
    def test_str(self):
        setUpTestData(cls=Scoreboard)
        self.highScores = HighScore()
        ret = self.highScores.__str__()
        tmp = ""
        for i in self.highScores.values:
            tmp_score = Scoreboard.objects.filter(score=i.score, name=i.name, datetime=i.datetime)[0]
            tmp += format(tmp_score.__str__())
            tmp += format("\n")
        self.assertEqual(ret, tmp)

