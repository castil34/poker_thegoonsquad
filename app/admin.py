from django.contrib import admin

from .models import User, Scoreboard

admin.site.register(User)
admin.site.register(Scoreboard)
