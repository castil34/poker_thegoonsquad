from django.test import TestCase
import app.pages.commandView as commandView
from app.game import Game
from app.models import User

# Jacob Howe
# PBI", "As a player, I want to be able to raise, call, check, or
# fold, so that I can be more in control of how much I win or lose.
# Size", "Large


class MakeMove(TestCase):
    # creates 2 players and adds them to table1
    def setUp(self):
        User.objects.create(name="admin")
        self.a = commandView.CommandView()
        commandView.goon_poker = Game()
        self.a.run_command("admin", "create table 1")
        self.a.run_command("admin", "add user asdf password")
        self.a.run_command("asdf", "join table 1")
        self.a.run_command("admin", "add user lkjh password")
        self.a.run_command("admin", "add user loser password")
        self.a.run_command("lkjh", "join table 1")
        self.a.run_command("loser", "join table 1")
        self.a.run_command("asdf", "check")
        self.a.run_command("lkjh", "check")

    # Acceptance Criteria: When I raise, the pot will increase by the amount raised and my
    # chips will reduce by the amount called.
    def test_raise(self):
        self.a.run_command("asdf", "chips")
        self.a.run_command("asdf", "raise 5")
        self.assertEqual("It is lkjh's turn\nasdf ;  total chips: 95 ; Current Bet:5 Status: raise", self.a.message())
        self.a.run_command("lkjh", "chips")
        self.assertEqual("lkjh has 100 chips", self.a.message())
        self.a.run_command("lkjh", "total pot")
        self.assertEqual("total pot is 5 chips", self.a.message())

    # Acceptance Criteria: When I fold, I will be removed from the round and I will not be
    # able to bet any more chips this round
    def test_fold(self):
        self.a.run_command("asdf", "fold")
        self.assertEqual("It is lkjh's turn\nasdf ;  total chips: 100 ; Current Bet:0 Status: Fold", self.a.message())
        self.a.run_command("asdf", "chips")
        self.assertEqual("asdf has 100 chips", self.a.message())

    # Acceptance Criteria: When I call, the pot will increase by the amount called and my
    # chips will reduce by the amount call/previous player raised
    def test_call(self):
        self.a.run_command("lkjh", "chips")
        self.a.run_command("asdf", "raise 5")
        self.a.run_command("lkjh", "call")
        self.assertEqual("It is loser's turn\nlkjh ;  total chips: 95 ; Current Bet:5 Status: call", self.a.message())
        self.a.run_command("lkjh", "chips")
        self.assertEqual("lkjh has 95 chips", self.a.message())
        self.a.run_command("lkjh", "total pot")
        self.assertEqual("total pot is 10 chips", self.a.message())

    # Acceptance Criteria: When I check, my chips will not be reduced, and the total pot
    # will not increase
    def test_check(self):
        self.a.run_command("lkjh", "chips")
        self.a.run_command("asdf", "check")
        self.a.run_command("lkjh", "check")
        self.assertEqual("It is loser's turn\nlkjh ;  total chips: 100 ; Current Bet:0 Status: Check", self.a.message())
        self.a.run_command("lkjh", "chips")
        self.assertEqual("lkjh has 100 chips", self.a.message())
        self.a.run_command("lkjh", "total pot")
        self.assertEqual("total pot is 0 chips", self.a.message())
