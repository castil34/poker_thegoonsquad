@ECHO OFF
REM install the coverage package first: pip install coverage
SETLOCAL
set excludes='app/migrations/*,app/tests/*,*/__init__.py,app/admin.py,app/apps.py,app/models.py,app/views.py'
coverage run --source='./app/' --omit=%excludes% manage.py test
coverage report
coverage html